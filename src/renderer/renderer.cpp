#include "renderer.hpp"

void renderer_init(RENDERER_BACKEND backend, SDL_Window* window) {
    main_backend = backend;
    if (main_backend == OPENGL) {
#ifdef OPENGL_SUPPORT
    opengl_init(window);
#else
        printf("Opengl support not compiled in\n");
        exit(1); // not implemented
#endif
    } else if (main_backend == VULKAN) {
        assert(0); // not implemented
    } else {
        assert(0); // not implemented
    }
}

void renderer_draw(rope_buffer buffer) {
    if (main_backend == OPENGL) {
#ifdef OPENGL_SUPPORT
        opengl_draw(buffer);
#else
        printf("Opengl support not compiled in\n");
        exit(1); // not implemented
#endif
    } else if (main_backend == VULKAN ) {
        assert(0); // not implemented
    } else {
        assert(0); // unreachable
    }
}
