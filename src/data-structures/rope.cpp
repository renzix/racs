#include "rope.hpp"
#include <cassert>
#include <cctype>

rope::RopeNode* rope::concat(RopeNode** left_node, RopeNode** right_node) {
    if (is_leaf(*left_node) && current_size(*left_node) == 0)
        return *right_node;
    if (is_leaf(*right_node) && current_size(*right_node) == 0)
        return *left_node;
    RopeNode* out_node = (RopeNode*) malloc(sizeof(RopeNode));
    if (out_node == nullptr) {
        printf("ERROR: Failed to malloc");
        return nullptr;
    }
    out_node->parent = nullptr;
    (*left_node)->parent = out_node;
    out_node->left_node = *left_node;
    (*right_node)->parent = out_node;
    out_node->right_node = *right_node;
    // if not null add the nodes together for the new node
    out_node->size = weight(out_node);
    out_node->left_location = 0;
    out_node->right_location = 0;
    return out_node;
}

uint64_t rope::weight(RopeNode* node) {
    if ((node->left_node == nullptr) && (node->right_node==nullptr))
        return current_size(node);
    else if (node->left_node == nullptr)
        return 0;
    else
        return weight(node->left_node, 0);
}

// Basically get the top left most node then go down right. Top left most node will have the size of everything to the left below it (so i only have to look at right)
uint64_t rope::weight(RopeNode* node, int num) {
    if (node == nullptr)
        return num;
    if (node->right_node!=nullptr)
        return weight(node->right_node, node->size+num);
    else if (node->left_node!=nullptr) // if it has something on the left its not a leaf
        return num+node->size;
    else
        return num+current_size(node);
}

/// SPLITS THE NODE ON location. Will still split if location is on a node.
/// @TODO(Renzix): make it not split if already on a node
rope::insert_return_t rope::split_or_add(RopeNode** node, RopeNode** split_tree, int location, char* str, int str_len) {
    assert(node!=nullptr);
    if ( (location < (*node)->size) && ((*node)->left_node!=nullptr) ) {
        if ((*node)->right_node!=nullptr) {
            // put right node on the stack so we dont lose it ( if it exists )
            stack::push(&rope::temp_stack,(*node)->right_node);
            // because are about to remove a node we have to update this nodes size
            // get size to remove from the parent nodes
            int size_removal = weight((*node)->right_node);
            assert((*node)->size >= 0);
            // @NOTE(Renzix): Go back up the list and subtract size_removal from size
            rope::RopeNode* old_node = (*node);
            while ((*node)->parent!=nullptr) {
                if ((*node)->parent->left_node == (*node))
                    (*node)->parent->size -= size_removal;
                (*node)=(*node)->parent;
                assert((*node)->size >= 0);
            }
            *node = old_node;

            // clear the right node for the split
            (*node)->right_node = nullptr;
        }
        // @SPEED(Renzix): maybe optimize this by setting node->parent->xxx =
        // node->child so I dont have to call rebalance in the future
        assert(rope::is_valid(*node));
        return rope::split_or_add(&(*node)->left_node, split_tree, location, str, str_len);
    }
    if ( (*node)->right_node!=nullptr ) {
        assert(rope::is_valid(*node));
        return rope::split_or_add(&(*node)->right_node, split_tree, (location - (*node)->size), str, str_len);
    }
    // @NOTE(Renzix): at this point we are at the correct location with **node
    // pointing to it. Lets try to see ifthere is room and add it if there is
    // some room, else split.
    assert((*node)->data!=nullptr);
    int bytes_added = buffer_insert(node, str, str_len, location);
    if (bytes_added == str_len) {
        current_length += bytes_added;
        return INSERTED;
    }
    // @NOTE(Renzix): Now we know there is no room to add it, lets see if we can
    // just make a new node and attach it to head because its on one of the ends
    if (bytes_added == 0)
        return ATTACH;
    // @NOTE(Renzix): At this point we know that we have to split the node in
    // order to insert something. To do this efficently we can just copy as much
    // data as we can to the node, then copy the rest to the new node.

    rope::RopeNode* right_node = (RopeNode*) malloc(sizeof(RopeNode));
    if (right_node == nullptr) {
        printf("ERROR: Failed to malloc right_node\n");
        return FAILURE;
    }
    right_node->data = (char*) malloc(MAX_NODE_SIZE*sizeof(char));
    if (right_node->data == nullptr) {
        printf("ERROR: Failed to malloc data\n");
        return FAILURE;
    }
    right_node->parent = nullptr;
    right_node->left_node = nullptr;
    right_node->right_node = nullptr;
    right_node->left_location = 0;
    right_node->right_location  = 0;
    int right_bytes_added = buffer_add(&right_node, str+bytes_added, str_len-bytes_added); // add whatever we didnt to the new string
    assert(right_bytes_added == str_len-bytes_added);
    current_length += str_len;

    // at this point we have split the poper node, now we can combine the right side nodes
    RopeNode* comb1 = right_node;
    RopeNode* comb2 = nullptr;
    int ret = 0;
    while((ret=stack::pop(&temp_stack, (void**) &comb2))!=0) {
        if (ret == 3) {
            // @TODO(Renzix): free the entire rope free_rope(comb2)
            continue;
        }
        comb1 = concat(&comb1, &comb2);
    }
    assert(rope::is_valid(comb1));
    *split_tree = comb1;
    return SPLIT_TREE;
}

bool rope::insert(RopeNode** node, const int location, char* cstr) {
    return rope::insert(node, location, cstr, strlen(cstr)-1);
}

/// @SPEED(Renzix): Use 128 bit nodes regardless of datasize (or bigger)
bool rope::insert(RopeNode** node, const int location, char* str, const int size) {
    // @TODO(Renzix): implement MAX_NODE_SIZE
    if (size > MAX_NODE_SIZE) {
        printf("ERROR: Trying to insert too big of a node");
        return false;
    }
    if ((current_length < location) || (location < 0)) {
        printf("ERROR: OUT OF BOUNDS INSERT\n");
        return false;
    }

    if (*node == nullptr) {
        *node = (RopeNode*) malloc(sizeof(RopeNode));
        if (*node == nullptr) {
            printf("ERROR: Failed to malloc");
            return false;
        }
        char* new_str = (char*) malloc(MAX_NODE_SIZE); // @TODO(Renzix): free this
        if (new_str == nullptr) {
            printf("ERROR: Failed to malloc");
            return false;
        }
        (*node)->data = new_str;
        (*node)->parent = nullptr;
        (*node)->left_node = nullptr;
        (*node)->right_node = nullptr;
        (*node)->left_location = 0;
        (*node)->right_location = 0;
        buffer_add(node, str, size);
        current_length += size;
        return true;
    }
    RopeNode** split_tree = (RopeNode**) malloc(sizeof(void*));
    insert_return_t ret = split_or_add(node, split_tree, location, str, size);
    if (ret == FAILURE) {
        return false;
    }
    if (ret == INSERTED) {
        assert(rope::is_valid(*node));
        return true;
    }
    //rebalance(node);
    // make a new node and add it to the og tree
    RopeNode* new_node = (RopeNode*) malloc(sizeof(RopeNode));
    if (new_node == nullptr) {
        printf("ERROR: Failed to malloc");
        return false;
    }
    char* new_str = (char*) malloc(MAX_NODE_SIZE); // @TODO(Renzix): free this
    if (new_node == nullptr) {
        printf("ERROR: Failed to malloc");
        return false;
    }
    memset(new_str, '\0', MAX_NODE_SIZE); // @TODO(DeBruno): remove
    new_node->data = new_str;
    new_node->parent = nullptr;
    new_node->left_node = nullptr;
    new_node->right_node = nullptr;
    new_node->left_location = 0;
    new_node->right_location = 0;
    buffer_add(&new_node, str, size);
    // if it properly split then concat the split part
    if (ret==SPLIT_TREE) {
        // get weight of node, if its more then &new_node then
        *node = concat(node, &new_node);
        // combine og tree with new tree
        *node = concat(node, split_tree); // combine the og node with the newly split node
    } else if (ret==ATTACH) { // if it didnt split then just concat the new node on the right
        *node = concat(node, &new_node);
    } else {
        return false;
    }
    current_length+=size;
    assert(rope::is_valid(*node));

    return true;
}

bool rope::remove(RopeNode** node, int left_location, int right_location) {
    if (node==nullptr || (*node)==nullptr) {
        return false;
    }
    if (((*node)->left_node == nullptr) && ((*node)->right_node == nullptr)) {
        return true;
    }
    return false;
}

// returns the node with location x
rope::RopeNode* rope::index(RopeNode* node, const int location) {
    if ( (location < node->size) && (node->left_node!=nullptr) ) {
        return rope::index(node->left_node, (location - node->left_node->size));
    } else if ( node->right_node!=nullptr ) {
        return rope::index(node->right_node, (location - node->right_node->size));
    }
    return node;
}

void rope::rebalance(RopeNode** node) {
    // first see if leaf node
    if (((*node)->left_node == nullptr) && ((*node)->right_node == nullptr))
        return;
    // if not leaf node then calc if its balanced, if not fix it
    if (abs(get_depth((*node)->left_node)-get_depth((*node)->right_node))>1) {
        // we have to balance this, idk how
    }
    // see if the depth of both sides are valid
    if ((*node)->left_node != nullptr) {
        rebalance(&((*node)->left_node));
    }
    if ((*node)->right_node != nullptr) {
        rebalance(&((*node)->right_node));
    }
}

int rope::get_depth(RopeNode* node) {
    return get_depth(node, 0);
}

#define MAX(a,b) (((a) > (b)) ? (a) : (b))
int rope::get_depth(RopeNode* node, int depth) {
    static int MAX_DEPTH = 0;
    if (node == nullptr)
        return depth;
    if ((node->left_node == nullptr) && (node->right_node == nullptr))
        return depth;
    if (node->left_node != nullptr) {
        MAX_DEPTH = MAX(get_depth(node->left_node, ++depth), MAX_DEPTH);
    }
    if (node->right_node != nullptr) {
        MAX_DEPTH = MAX(get_depth(node->right_node, ++depth), MAX_DEPTH);
    }
    return MAX_DEPTH;
}

void rope::print(RopeNode* node) {
    print(node, 0);
}

void rope::print(RopeNode* node, int depth) {
    if (node == nullptr)
        return;
    if ((node->left_node == nullptr) && (node->right_node == nullptr)) {
        buffer_print(node);
        return;
    }
    if (node->left_node != nullptr) {
        rope::print(node->left_node, ++depth);
    }
    if (node->right_node != nullptr) {
        rope::print(node->right_node, ++depth);
    }
}

// okay so a node is valid if node is nullptr, parent is larger (if parent exists), if both children (if exists) are valid
bool rope::is_valid(RopeNode* node) {
    if (node == nullptr)
        return true;
    if (node->size < 0) { //@TODO(Renzix): make this think 0 is not valid
        printf("SIZE NEGITIVE\n");
        return false;
    }
    if ((node->left_node == nullptr) && (node->right_node == nullptr)) {
        // @TODO(Renzix): circular buffer
        for (int i = 0; i < node->right_location-node->left_location; i++) {
            if (node->data == nullptr) {
                printf("NULL STRING");
                return false;
            }
            if (isprint(!(*(node->data+i)))) {// if characters arent printable
                printf("NOT PRINTABLE\n");
                return false;
            }
            if (MAX_NODE_SIZE < rope::current_size(node)) {
                printf("Size of node is not the same as the right/left\n");
                return false;
            }
        }
    } else if (node->size != weight(node)) { // if its not a child
        printf("The weight of the nodes are fucked up\n");
        return false;
    }

    if (node->left_node != nullptr) {
        if (node->left_node->size > node->size) {// if left child is larger then parent
            printf("LEFT CHILD LARGER THEN PARENT");
            return false;
        }
        if (!(is_valid(node->left_node))) {// if child isnt valid return false
            return false;
        }
    }
    if (node->right_node != nullptr)
        if (!(is_valid(node->right_node))) // if child isnt valid return false
            return false;
    return true;
}

int rope::buffer_insert(RopeNode** node, char* str, int str_len, int location) {
    assert(node!=nullptr);
    assert((*node)!=nullptr);
    assert((*node)->left_node==nullptr);
    assert((*node)->right_node==nullptr);
    // if we dont have room then return 0
    if ((MAX_NODE_SIZE < location) || (location < 0)) {
        assert(1);
        return 0;
    }
    if (MAX_NODE_SIZE < (current_size(*node) + str_len))
        return 0;
    // first make room for the insertion
    int new_right = (*node)->right_location+str_len%MAX_NODE_SIZE;
    int old_right = (*node)->right_location;
    while(location != new_right) {
        printf("%i\n",old_right);
        old_right %= MAX_NODE_SIZE;
        new_right %= MAX_NODE_SIZE;
        (*node)->data[new_right--] = (*node)->data[old_right--];
    }
    // now actually insert stuff
    for(int i = 0; i<str_len; i++) {
        (*node)->data[i+location+(*node)->left_location] = str[i];
    }
    (*node)->right_location += str_len;
    (*node)->right_location %= MAX_NODE_SIZE;
    return str_len;
}

int rope::buffer_add(RopeNode** node, char* str, int str_len) {
    return buffer_insert(node, str, str_len, current_size(*node));
}

void rope::buffer_delete(RopeNode** node, int start, int len) {
    assert(node!=nullptr);
    assert(is_leaf(*node));
    assert(start >= 0 && start <=MAX_NODE_SIZE);
    assert(len   >  0 && len   <=MAX_NODE_SIZE);
    int new_left = (start+(*node)->left_location+len)%MAX_NODE_SIZE;
    int old_left = (*node)->left_location+start;
    // @TODO(Renzix): take everything to the right of delete_right and move it
    // over to delete_left
    for (int i = current_size(*node)-len; i>0 ;i--) {
        old_left %= MAX_NODE_SIZE;
        new_left %= MAX_NODE_SIZE;
        (*node)->data[old_left++] = (*node)->data[new_left++];
    }
    (*node)->right_location -= len;
    (*node)->right_location %= MAX_NODE_SIZE;
}

void rope::buffer_print(RopeNode* node) {
    assert(node!=nullptr);
    assert(is_leaf(node));
    for (int i = node->left_location; i!=node->right_location; i++) {
        printf("%c", node->data[i]);
        i %= MAX_NODE_SIZE;
    }
}

int rope::current_size(RopeNode* node) {
    assert(node!=nullptr);
    assert(is_leaf(node));
    if (node->right_location >= node->left_location)
        return node->right_location - node->left_location;
    else
        return (MAX_NODE_SIZE-node->right_location) + node->left_location;
}

bool rope::is_leaf(RopeNode* node) {
    if (node == nullptr) return false;
    if (node->left_node  != nullptr) return false;
    if (node->right_node != nullptr) return false;
    return true;
}
