#ifndef FONT_H_
#define FONT_H_
#include <cstdio>
#include <cstdlib>

#ifdef FREETYPE_SUPPORT
#include "freetype.hpp"
#endif
#include "stb_font.hpp"

namespace font {
  typedef enum font_type {
    bitmap,
    vector
  } font_type;

  typedef enum FONT_BACKEND {
    STB_TRUETYPE,
    FREETYPE
  } FONT_BACKEND;

  void font_init(FONT_BACKEND backend);
  void font_free(FONT_BACKEND backend);

  extern unsigned char* font_buffer;
}
#endif // FONT_STB_TRUETYPE_H_
