#include "stack.hpp"


uint_fast8_t stack::pop(StackNode** head, void** data) {
    if (*head == nullptr) {
        return 0; // dont do anything
    } else if ((*head)->next == nullptr) {
        *data = (*head)->data;
        free(*head);
        *head = nullptr;
        return 2; // skip the pop and just give data
    } else if ((*head)->data == nullptr) {
        StackNode* next_node = (*head)->next;
        free(*head);
        *head = next_node;
        return 3; // skip the data and just pop (got 0)
    }

    *data = (*head)->data;
    StackNode* next_node = (*head)->next;
    free(*head);
    *head = next_node;
    return 1; // pop and give data
}

void stack::push(StackNode** head, void* data) {
    if (head==nullptr) {
        *head = (StackNode*) malloc(sizeof(StackNode));
        (*head)->data = data;
        return;
    }
    // create new head, set this head to point to new head
    StackNode* new_head = (StackNode*) malloc(sizeof(StackNode));
    if (new_head == nullptr) {
        printf("ERROR: COULD NOT PUSH TO STACK");
        return;
    }
    new_head->next = *head;
    new_head->data = data;
    *head = new_head;
}

// Does not free void*'s because i did not allocate them
void stack::destroy(StackNode** head) {
    void** data;
    while (pop(head, data));
}
