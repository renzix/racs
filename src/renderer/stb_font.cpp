#include "stb_font.hpp"


/// Opens the font and sets the font struct
void stb_truetype::stb_font_init() {
  using font::font_buffer;
  // font generation stuff
  long font_size;

  FILE *font_file = fopen("/usr/share/fonts/noto/NotoSans-Regular.ttf", "rb");
  fseek(font_file, 0, SEEK_END);
  font_size = ftell(font_file); /* how long is the file ? */
  rewind(font_file);            /* reset */

  font_buffer = (unsigned char *)malloc(font_size);
  fread(font_buffer, font_size, 1, font_file);
  fclose(font_file);
}
