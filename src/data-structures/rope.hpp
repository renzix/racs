#ifndef ROPE_H_
#define ROPE_H_

// std libs
#include <cstdint>
#include <cstdlib>

// my libs
#include "stack.hpp"

namespace rope {
    enum insert_return_t {
        FAILURE,
        ATTACH,
        INSERTED,
        SPLIT_TREE
    };

    struct RopeNode {
        RopeNode* parent;
        RopeNode* left_node;
        RopeNode* right_node;
        int size; // if parent then left node size else if child then max size
        uint8_t left_location;  // only relevant for children
        uint8_t right_location; // only relevant for children
        char* data; // note this is not null terminated
    };

    static stack::StackNode* temp_stack = nullptr;
    const uint_fast8_t MAX_NODE_SIZE = 8;
    static uint64_t current_length = 0;

    RopeNode* concat(RopeNode** node1, RopeNode** node2);
    insert_return_t split_or_add(RopeNode** node, RopeNode** split_node, int location, char* str, int str_len);
    uint64_t weight(RopeNode* node);
    uint64_t weight(RopeNode* node, int num);
    void rebalance(RopeNode** node);
    int get_depth(RopeNode* node);
    int get_depth(RopeNode* node, int depth);
    void remove_size(RopeNode& node, int value);

    bool insert(RopeNode** node, const int location, char* str);
    bool insert(RopeNode** node, const int location, char* str, int size);
    RopeNode* index(RopeNode* node, const int location);

    bool remove(RopeNode** node, int left_pos, int right_node);

    // leaf node stuff
    int  buffer_insert(RopeNode** node, char* str, int str_len, int location);
    int  buffer_add(RopeNode** node, char* str, int str_len);
    void buffer_delete(RopeNode** node, int start, int len);
    void buffer_print(RopeNode* node);
    int  current_size(RopeNode* node);
    bool is_leaf(RopeNode* node);

    // Debug stuff
    void print(RopeNode* node);
    void print(RopeNode* node, int depth);
    bool is_valid(RopeNode* node);
};


#endif // ROPE_H_
