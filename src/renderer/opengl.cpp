#include "opengl.hpp"
// for fonts
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.hpp"

unsigned char temp_bitmap[1024*1024];
stbtt_bakedchar cdata[96]; // ASCII 32..126 is 95 glyphs
GLuint ftex;

bool opengl_init(SDL_Window* w) {
    window = w;
    context = SDL_GL_CreateContext(window);
    // clear the window and make it white
    glClearColor(1.0,1.0,1.0,1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    SDL_GL_SwapWindow(window);

    font::font_init(font::STB_TRUETYPE);

    stbtt_BakeFontBitmap(font::font_buffer, 0, 24.0, temp_bitmap, 512, 512, 32, 96, cdata); // no guarantee this fits!

    glGenTextures(1, &ftex);
    glBindTexture(GL_TEXTURE_2D, ftex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, 512,512, 0, GL_ALPHA, GL_UNSIGNED_BYTE, temp_bitmap);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // stbtt_FreeBitmap(temp_bitmap, nullptr);

    return true;
}

bool opengl_draw(rope_buffer buffer) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,renderer_height,renderer_width,0,-1,1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    opengl_draw_rope_buffer(buffer.rope);
    //opengl_print_cstr(50,50,"hello world!!!");

    SDL_GL_SwapWindow(window);
    return true;
}

void opengl_draw_rope_buffer(rope::RopeNode* rope) {
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor3f(0,0,0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, ftex);
    glBegin(GL_QUADS);
    render_cursor_x = 5;
    render_cursor_y = 20;
    opengl_draw_rope_buffer_chars(rope);
    glEnd();
}

// @TODO(Renzix): create array and render it all at once instead of one at a time (instead of 1 at a time as shown below)
void opengl_draw_rope_buffer_chars(rope::RopeNode* node) {
  if (node == nullptr)
    return;
  if ((node->left_node == nullptr) && (node->right_node == nullptr) && (node->size >= 0)) {
    for(int i = 0; i < node->size; i++) {
      if (*(node->data+i) >= 32) {
        stbtt_aligned_quad q;
        stbtt_GetBakedQuad(cdata, 512,512, *(node->data+i)-32, &render_cursor_x ,&render_cursor_y ,&q,1);//1=opengl & d3d10+,0=d3d9
        glTexCoord2f(q.s0,q.t0); glVertex2f(q.x0,q.y0);
        glTexCoord2f(q.s1,q.t0); glVertex2f(q.x1,q.y0);
        glTexCoord2f(q.s1,q.t1); glVertex2f(q.x1,q.y1);
        glTexCoord2f(q.s0,q.t1); glVertex2f(q.x0,q.y1);
      }
    }
    return;
  }
  if (node->left_node != nullptr) {
    opengl_draw_rope_buffer_chars(node->left_node);
  }
  if (node->right_node != nullptr) {
    opengl_draw_rope_buffer_chars(node->right_node);
  }
}
