# Ren

I always wanted to make a text editor so here we go. There will be a lot of
inspiration from other text editors (mainly emacs). My goal with this is to make
a editor which is written from the ground up to be fast and provide multiple
tools to make programming and managing your system easier. I want this to be a 
place which letsyou interface with anything.

SEE CI FOR DOWNLOAD! May make a app image if this actually gets good/used

## Why this over another editor

I have realized that I will not be able to compete with emacs or vim through
sheer customization(and likely never will). Because of this I have decided to
make a editor which solves some of the pain points in both programs, some of
those being
    1. Older codebase which supports MANY different systems
    2. Caters towards everyone
    3. Originally based on tui
    4. Slow when heavy scripting/customization
Because of this I am making this a gui application which is hardware accelerated
and will not be making a scripting language. If you want to edit this, you must
edit the source code which I hopefully have made readable and extendable.

The other thing I want to do with this text editor is experiment with different
keybinding philosophies. I have used many different editors and want to add some
of these ideas into my own frankenstein text editor. Some of these being heavy
emphasis on contextual keybindings(thanks org mode), maybe mouse editing (thanks
acme), tui like interfaces (thanks magit), modal editing (thanks vim) and much
much more.

## How it works

### Rendering

My plan is to use Software rendering through SDL or opengl for now. Might
consider using other things like vulkan or native x11/wayland but rendering text
is pretty light and both of those require a lot of work. Currently only opengl
is implemented.

### Font Rendering

For now I am using stb_truetype but am planning on adding freetype support too. 

### Text Storage

We will support multiple buffer types but I wanted to start out with the rope
first (because the concept seems cool af). For now all arrays will store raw
chars but I am still thinking if I want to implement `uint32_t` or `uint8_t` for
utf8s (fast indexing vs less memory usage), I am leaning towards just using
`uint32_t` for simplicity.

#### Rope buffer
As of the current commit every letter pressed dynamically allocates more memory
however I plan on changing that to dynamically allocating 256 bytes at a time.
When inserting into the rope it will just add those characters directly to the
array unless there is no room. Note the array will be implemented as a circular
buffer.

#### Other planned buffer types

 - array buffer (array of arrays): for basic gui/tui and not storing of files
 - Pierce Table: To provide alternative to rope. Probably not going to do as it
   would require some work.

### Planned Features/Miniprograms (no where near getting done lol)

Note some of these will be installed along side the editor as !#/bin/sh scripts
which just calls `racs --rt` or `racs --rde` something. Also these are just
thought experiments and nowhere near getting done.

#### rde (Directory Editor or desktop environment xd) 

I plan on this being the killer feature of racs and will probably change from
the ideas here over time. I find twin pane file managers extremely interesting
but they all have pretty bad keybindings by default so I never end up using any
of them. It also becomes more interesting when you look at things like emacs
helm where you could do file management right then and there. I want to combine
these 2 ideas into a modal file manager which functions more like helm in
"insert mode" and more like a file manager in "normal mode". To add to this, I
want to include project management into this somehow. This should in my opinion
relace the terminal unless you REALLY need the terminal like how magit replaced
git for me.

#### rt (terminal)

Okay so this will be the terminal emulator for racs. I think terminal emulators
in most editors are just bad so normally I just have a terminal on another
desktop. I want to be able to have a terminal emulator which will look decent
and work like a normal terminal emulator for now. I do not like how vim and
emacs implement terminal emulators at all. The way vscode does a drop down
terminal is alright but imo its a crutch for not being able to do other things
like build or manage files.

#### rmd (Markdown editor)

This will likely never get close to the functionality of what org mode has done
but I want to do a basic markdown mode with some other options like running
code, creating links, contextual note taking and more.

#### ragit

Magit clone for racs. Please DONATE to Jonas Bernoulli https://magit.vc/donate/
because he makes awesome software and this software wouldn't be here without him.

## Contributing

I would love if you the reader would like to contribute and if this project
actually gets good then I probably will write a contribute.md file. As of right
now just say 1. What you are doing and 2. Why its wanted/needed. If there are
other ways to do things I may ask why you are doing things this way. Lastly try
to follow the general tone of the project ie no OOP, very little library use (if
so it should be optional), very C like C++ with little modern features etc.

## Build

I made a script because why use make. Might think about writing something which
is crossplatform.  

./b build    - compile and link debug version
./b compile  - compile debug
./b link     - link debug
./b clean    - delete all obj/dep/bin files
./b run      - compile, link and run the debug version
./b release  - build release version
./b install  - builds release and installs file to $PREFIX/bin

### Dependencies

Built with C++11 but uses very few features from it (we use nullptr for
example).

For libraries we currently use
    - libsdl2
    - opengl (optional but currently required)
    - freetype (optional but currently required)
