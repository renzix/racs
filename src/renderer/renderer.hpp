#ifndef RENDERER_H_
#define RENDERER_H_

// std
#include <assert.h>
#include <stdbool.h>

// my libs
#include "opengl.hpp"

// 3rd party libs
#include <SDL2/SDL.h>

typedef enum RENDERER_BACKEND {
  OPENGL,
  VULKAN
} RENDERER_BACKEND;

void renderer_init(RENDERER_BACKEND backend, SDL_Window* window);
void renderer_draw(rope_buffer buffer);

static RENDERER_BACKEND main_backend = OPENGL;

#endif // RENDERER_H_
