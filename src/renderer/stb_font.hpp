#ifndef STB_FONT_H_
#define STB_FONT_H_

#include <cstdio>
#include <cstdlib>

#include "font.hpp"

namespace stb_truetype {
    void stb_font_init();
}

extern unsigned char* font_buffer;

#endif // STB_FONT_H_
