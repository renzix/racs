#include "rope_buffer.hpp"

void rope_buffer::init() {
  rope = nullptr;
}

void rope_buffer::cursor_move(int64_t distance) {
  //printf("\nCursor: %lu\nDistance: %li", cursor, distance);
  if ((distance > static_cast<int64_t>(cursor)) && (distance < 0)) {
#ifdef DEBUG
    printf("Trying to move out of bounds (distance negitive and too far)\n");
#endif
    return;
  }
  if (((cursor + distance) <= screen_bottom_right) && ((cursor+distance) >= screen_top_left)) {
#ifdef DEBUG
    printf("Cursor: %lu moved to %lu\n", cursor, cursor+distance);
#endif
    cursor += distance;
  } else {
#ifdef DEBUG
      printf("Trying to move out of bounds\n");
#endif
  }

}

// assumes text is nullt terminated
void rope_buffer::insert_text_at_cursor(char* cstr) {
  insert_text_at_cursor(cstr, strlen(cstr));
}

void rope_buffer::insert_text_at_cursor(char* str, int size) {
  bool ret = rope::insert(&rope, cursor, str, size);
  if (ret==false) {
    printf("WARNING: FAILURE TO INSERT\n");
    return;
  }
  cursor += size;  // @TODO(Renzix): check for bounds
  screen_bottom_right += size;
#ifdef DEBUG
  assert(rope::is_valid(rope));
  printf("Rope is now '"); rope::print(rope); puts("'");
  printf("Cursor: %i\n", cursor);
  printf("screen_bottom_right: %i\n", cursor);
#endif
}

void rope_buffer::remove(int amount) {
  if (amount > 0) {
    rope_buffer::remove(cursor, amount+cursor);
  } else if (amount < 0) {
    rope_buffer::remove(cursor+amount, cursor);
  } else {
    printf("Tried to delete 0\n");
  }
}
void rope_buffer::remove(int left_pos, int right_pos) {
  bool ret = rope::remove(&rope, left_pos, right_pos);
  if (!ret) {
    printf("Did not delete anything\n");
  }
}
