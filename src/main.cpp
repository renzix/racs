// std
#include <stdio.h>
#include <stdbool.h>

// my libs
#include "buffers/rope_buffer.hpp"
#include "renderer/renderer.hpp"
#include "data-structures/rope.hpp"

// 3rd party libs
#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keyboard.h>

const int INIT_WIDTH = 800;
const int INIT_HEIGHT = 600;
bool running = true;

const RENDERER_BACKEND backend = OPENGL;

// @TODO(Renzix): remove these items
rope_buffer myrope_buffer;
static char temp_buf[128] = {0};

int deal_with_keypress(SDL_Keysym sym);
int open_file(char* path);


int main(void) {
    myrope_buffer.init();
    myrope_buffer.insert_text_at_cursor("hello ");
    //myrope_buffer.insert_text_at_cursor(" name ");
    //myrope_buffer.insert_text_at_cursor("is ren");
    //myrope_buffer.insert_text_at_cursor("zix!!!");
    //myrope_buffer.cursor_move(-10);
    //myrope_buffer.insert_text_at_cursor("1");
    //myrope_buffer.cursor_move(2);
    //myrope_buffer.insert_text_at_cursor("2");
    //myrope_buffer.cursor_move(-8);
    //myrope_buffer.insert_text_at_cursor("3");
    //myrope_buffer.cursor_move(2);
    //myrope_buffer.insert_text_at_cursor("4");
    myrope_buffer.insert_text_at_cursor("ren");
    myrope_buffer.insert_text_at_cursor("zix");
    myrope_buffer.insert_text_at_cursor("!!!");
    myrope_buffer.cursor_move(-4);
    myrope_buffer.insert_text_at_cursor("6");
    myrope_buffer.cursor_move(-1);
    myrope_buffer.insert_text_at_cursor("!!!");
    return 0;
    //init sdl stuff
    SDL_Event e = { 0 };
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
    uint32_t sdl_window_flags = SDL_WINDOW_OPENGL|SDL_WINDOW_SHOWN;
    SDL_Window* window = SDL_CreateWindow( "Racs",
                   SDL_WINDOWPOS_UNDEFINED,
                   SDL_WINDOWPOS_UNDEFINED,
                   INIT_WIDTH, INIT_HEIGHT, sdl_window_flags);


    renderer_init(backend, window);

    SDL_StartTextInput();
    while (running) {
        // First dislay
        // @TODO(Renzix): Draw buffer
        renderer_draw(myrope_buffer);

        // Now get input
        // @TODO(Renzix): More input
        while(SDL_PollEvent(&e)){
            switch (e.type) {
                case SDL_KEYUP: break;
                case SDL_KEYDOWN:
                    deal_with_keypress(e.key.keysym);
                    break;
                case SDL_TEXTINPUT:
                    // dunno if useful
                    break;
                case SDL_QUIT:
                    running = false;
                    break;
                default:
                    break;
            }
        }
        // Finally do random needed tasks
        SDL_Delay(10);
    }

    // quit sdl
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}

// char* SOME_TEXT = "HELLO";
/// TODO(DeBruno): Going to replace with map in vm?
int deal_with_keypress(SDL_Keysym sym) {
    if ((sym.sym>=32)&&(sym.sym<=127)) {
        snprintf(temp_buf, 2, "%c", sym.sym);
        myrope_buffer.insert_text_at_cursor(temp_buf, 1);
        return 0;
    }
    switch (sym.sym) {
        case SDLK_UP:
            // @TODO(Renzix): implement
            break;
        case SDLK_DOWN:
            // @TODO(Renzix): implement
            break;
        case SDLK_LEFT:
            myrope_buffer.cursor_move(-1);
            break;
        case SDLK_RIGHT:
            myrope_buffer.cursor_move(1);
            break;
        case SDLK_BACKSPACE:
            myrope_buffer.remove(-1);
            break;
        case SDLK_ESCAPE:
            running = false;
            break;
        default:
            printf("No key bound to %c\n", sym.sym);
            return 1;
            break;
    }
    return 0;
}

int open_file(char* path) {
    return 1;
}
