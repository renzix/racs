#ifndef ROPE_BUFFER_H_
#define ROPE_BUFFER_H_

#include <cstdint>
#include <cstdio>
#include <cassert>

#include "../data-structures/rope.hpp"
static const size_t screen_lines_number = 50; // @TODO(Renzix): make dynamic

typedef struct rope_buffer {
  // array of all the endings of the lines location in the rope
  // could prob use vector but no stl
  size_t screen_lines_current = 0;
  const uint64_t screen_line_ends[screen_lines_number] = {0};
  uint64_t screen_top_left = 0;
  uint64_t screen_bottom_right = 0;

  rope::RopeNode* rope = {0};

  uint64_t cursor = 0;

  void init();
  void insert_text_at_cursor(char* cstr);
  void insert_text_at_cursor(char* str, int size);
  void remove(int amount);
  void remove(int left_pos, int right_pos);

  void cursor_move(int64_t distance);
}rope_buffer;
#endif // ROPE_BUFFER_H_
