#ifndef OPENGL_H_
#define OPENGL_H_

// std
#include <stdbool.h>

// system libs
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GL/gl.h>

#include "../data-structures/rope.hpp"
#include "../buffers/rope_buffer.hpp"

#include "font.hpp"

bool opengl_init(SDL_Window* window);
bool opengl_draw(rope_buffer buffer);
void opengl_draw_rope_buffer(rope::RopeNode* rope);
void opengl_draw_rope_buffer_chars(rope::RopeNode* node);

static SDL_GLContext context;
static SDL_Window* window;

static int renderer_height = 800;
static int renderer_width = 600;

static float render_cursor_x;
static float render_cursor_y;

// @TODO(DeBruno): remove this
extern unsigned char* font_buffer;

#endif // OPENGL_H_
