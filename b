#!/bin/sh

set -e

## VARIABLES
BIN=racs
USED_LIBS="sdl2 gl freetype2" # @TODO(Renzix): if any lib contains gl then it will get opengl, need to fix that

CC=g++
CFLAGS="-std=c++11 -static -DDEBUG $(pkg-config --cflags --libs ${USED_LIBS})"

LINKER=g++
LIBS="$(pkg-config --libs ${USED_LIBS})"
LFLAGS="${LIBS}"

OBJDIR="obj"

## source, objects and dependencies
SOURCE="
src/renderer/renderer.cpp
src/renderer/font.cpp
src/renderer/stb_font.cpp
src/data-structures/stack.cpp
src/data-structures/rope.cpp
src/buffers/rope_buffer.cpp
src/main.cpp
"

case ${USED_LIBS} in
    *freetype2*)   DEFINES="${DEFINES} -DFREETYPE_SUPPORT"
                   SOURCE="${SOURCE} src/renderer/freetype.cpp"
                   ;;
esac
case ${USED_LIBS} in
    *gl*)   DEFINES="${DEFINES} -DOPENGL_SUPPORT"
            SOURCE="${SOURCE} src/renderer/opengl.cpp"
            ;;
esac

for val in ${SOURCE}
do
    TEMP="${OBJDIR}/${val#*src/}"
    TEMP="$(printf ${TEMP} | sed 's/\.cpp/.o/g')"
    OBJECTS="${OBJECTS} ${TEMP}"
done
DEPENDENCY="$(echo ${OBJECTS} | sed 's/\.o/.d/g')"

## USEFUL STUFF
err() {
    printf "%s\n" "$1"
    exit 1
}

## BUILD STUFF
compile() {
    for cfile in ${SOURCE}
    do
        ofile="${OBJDIR}/${cfile#*src/}"
        mkdir -p "${ofile%/*}"
        if [  "${1}" = "RELEASE" ] # if RELEASE
        then
            ${CC} ${CFLAGS} ${DEFINES} -c ${cfile} -o ${ofile%.*}.o
        else
            ${CC} ${CFLAGS} ${DEFINES} -MMD -c -g ${cfile} -o ${ofile%.*}.o
        fi
    done
    printf "Build complete\n"
}

link() {
    mkdir -p ${BINDIR}
    ${LINKER} ${OBJECTS} ${LFLAGS} -o ${BINDIR}/${BIN}
}

clean() {
    find obj -name '*.o' -exec rm {} \;
    find obj -name '*.d' -exec rm {} \;
    rm -rf ${BINDIR:=bin}/*
    printf "Clean complete\n"
}

## AGUMENTS CHECKING
if [ -z "$1" ]
then
    err "No argument" 1
fi

if [ -n "$2" ]
then
    err "Too many arguments" 1
fi

case $1 in
    build) BINDIR="bin/debug"
           compile
           link
           ;;
    compile) compile
             ;;
    link) BINDIR="bin/debug"
          link
          ;;
    clean) clean
           ;;
    run) BINDIR="bin/debug"
         compile
         link
         ${BINDIR}/${BIN}
         ;;
    release) BINDIR="bin/release"
             compile RELEASE
             link
             ;;
    install) BINDIR="bin/release"
             compile
             link
             sudo install ${BINDIR}/${BIN} ${PREFIX:=/usr/local}/bin/${BIN}
             chmod 755 ${BINDIR}/${BIN}
             ;;
    *) err "Incorrect argument: $1" 1
       ;;
esac

