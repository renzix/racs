#ifndef STACK_H_
#define STACK_H_

#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <cstdio>

// @TODO(Renzix): convert to template for data
namespace stack {
    struct StackNode {
        void* data;
        StackNode* next;
    };

    uint_fast8_t pop(StackNode** head, void** data);
    void push(StackNode** head, void* data);
    void destroy(StackNode** head);

}

#endif // STACK_H_
