#include "font.hpp"

unsigned char* font::font_buffer = nullptr;

void font::font_init(FONT_BACKEND backend) {
    if (backend == STB_TRUETYPE) {
        stb_truetype::stb_font_init();
    } else if (backend == FREETYPE) {
#ifdef FREETYPE_SUPPORT
        freetype_init();
#else
        printf("Not compiled with freetype\n");
        exit(1);
#endif
    } else {
        printf("Unimplementetd!\n");
        exit(1);
    }
    return;
}
void font::font_free(FONT_BACKEND backend) {
    if (font_buffer != nullptr)
        free(font_buffer);
}
